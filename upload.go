package main

import (
	"compress/gzip"
	"fmt"
	"io"
	"io/ioutil"
	"net/url"
	"path"
	"time"

	"bitbucket.org/6thdigital/pg-cloud-backup/config"

	log "github.com/Sirupsen/logrus"

	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/aws/aws-sdk-go/service/s3/s3manager"

	"golang.org/x/net/context"
	"golang.org/x/oauth2/google"
	"google.golang.org/cloud"
	"google.golang.org/cloud/storage"
)

const (
	GoogleCloudStoragePublicUrlPrefix = "https://storage.googleapis.com"

	ReqTimeout = 5 * time.Second
)

var (
	googleCloudAuthData []byte

	bgCtx = context.Background()
)

// uploadFileToS3 will gzip compress the file and upload it to the desired bucket in S3
func uploadFileToS3(filename string, file io.ReadCloser) error {
	filekey := path.Base(filename) + ".gz"

	// Not required, but you could zip the file before uploading it
	// using io.Pipe read/writer to stream gzip'd file contents.
	reader, writer := io.Pipe()
	go func() {
		gw := gzip.NewWriter(writer)
		io.Copy(gw, file)

		file.Close()
		gw.Close()
		writer.Close()
	}()

	uploader := s3manager.NewUploader(session.New(&aws.Config{Region: aws.String(config.GetS3Region())}))

	result, err := uploader.Upload(&s3manager.UploadInput{
		Body:   reader,
		Bucket: aws.String(config.GetS3Bucket()),
		Key:    aws.String(filekey),
	})

	if err != nil {
		log.Warnln("Failed to upload", err)
		return err
	}

	log.Infoln("Successfully uploaded to", result.Location)

	return nil
}

// getStorageFullScopeService
func getStorageFullScopeService() (*storage.Client, error) {
	timeoutCtx, cancel := context.WithTimeout(bgCtx, ReqTimeout)
	defer cancel() // releases resources if slowOperation completes before timeout elapses

	if len(googleCloudAuthData) == 0 {
		var err error
		googleCloudAuthData, err = ioutil.ReadFile(config.GetGoogleCloudAuthJson())
		if err != nil {
			googleCloudAuthData = []byte{}
			return nil, err
		}
	}

	conf, err := google.JWTConfigFromJSON(
		googleCloudAuthData,
		storage.ScopeFullControl,
	)

	if err != nil {
		googleCloudAuthData = []byte{}
		return nil, err
	}

	return storage.NewClient(timeoutCtx, cloud.WithTokenSource(conf.TokenSource(timeoutCtx)))
}

func buildGCSPublicUrl(bucket, object string) string {
	u, _ := url.Parse(GoogleCloudStoragePublicUrlPrefix)
	u.Path = path.Join(u.Path, bucket, object)
	return u.String()
}

func uploadToGoogleCloudStorage(filename string, r io.ReadCloser) error {
	ctx, cancel := context.WithCancel(bgCtx)
	defer cancel() // releases resources when finished

	service, err := getStorageFullScopeService()

	if err != nil {
		return err
	}

	// Not required, but you could zip the file before uploading it
	// using io.Pipe read/writer to stream gzip'd file contents.
	reader, writer := io.Pipe()
	go func() {
		gw := gzip.NewWriter(writer)
		io.Copy(gw, r)

		r.Close()
		gw.Close()
		writer.Close()
	}()

	filekey := path.Base(filename) + ".gz"
	bucketName := config.GetGoogleCloudStorageBackupBucket()

	objName := fmt.Sprintf("pg-cloud-backups/%s", filekey)

	fw := service.Bucket(bucketName).Object(objName).NewWriter(ctx)

	if b, err := io.Copy(fw, reader); err == nil {
		log.Infof("Wrote: %d bytes to google cloud storage", b)
	} else {
		log.Infof("Could not write to google cloud storage: %v", err)
		return err
	}
	fw.Close()
	reader.Close()

	log.Infof("Successfully uploaded to: '%s'", buildGCSPublicUrl(bucketName, objName))

	return nil
}
