package config

import (
	"path/filepath"

	log "github.com/Sirupsen/logrus"

	"github.com/spf13/pflag"
	"github.com/spf13/viper"
)

func ParseConfig(args []string) error {
	log.Debugf("Parsing configuration")

	viper.SetConfigName("pgcloudbackup")         // name of config file (without extension)
	viper.AddConfigPath("/etc/pgcloudbackup/")   // path to look for the config file in
	viper.AddConfigPath("$HOME/.pgcloudbackup/") // call multiple times to add many search paths
	viper.AddConfigPath(".")                     // optionally look for config in the working directory
	viper.AddConfigPath("./config/")
	err := viper.ReadInConfig() // Find and read the config file
	if err != nil {             // Handle errors reading the config file
		log.Errorf("Fatal error config file: %s \n", err)
		return err
	}

	flags := pflag.NewFlagSet("pgcloudbackup", pflag.ContinueOnError)
	flags.StringP("host", "h", "localhost", "Host the database is running on")
	flags.IntP("port", "p", 5432, "Port the database is running on")
	flags.StringP("dbuser", "u", "pgsql", "User to connect to database")
	flags.StringP("dbpasswd", "P", "", "Password to connect to database")
	flags.StringP("dbname", "d", "", "The name of the databse to backup")
	flags.StringP("loglevel", "l", "debug", "Logging Level, Valid values: debug, info, warning, error, fatal, panic")
	flags.String("logpath", "stdout", "The path to save the log files in.")
	flags.StringP("fileprefix", "f", "pg_backup", "The prefix for the output backup file")
	flags.StringP("bucket", "b", "bucket1", "The bucket to upload the file to")
	flags.StringP("region", "r", "us-east-1", "The AWS region code that the bucket is in")
	flags.StringP("cache", "c", "/tmp", "The cache directory to put the temp file in before uploading")
	flags.BoolP("deleteafterupload", "m", true, "Delete the temp file after successful upload")

	viper.BindPFlag("database.host", flags.Lookup("host"))
	viper.BindPFlag("database.port", flags.Lookup("port"))
	viper.BindPFlag("database.user", flags.Lookup("dbuser"))
	viper.BindPFlag("database.password", flags.Lookup("dbpasswd"))
	viper.BindPFlag("database.name", flags.Lookup("dbname"))
	viper.BindPFlag("log.dir", flags.Lookup("logpath"))
	viper.BindPFlag("log.level", flags.Lookup("loglevel"))
	viper.BindPFlag("backup.fileprefix", flags.Lookup("fileprefix"))
	viper.BindPFlag("backup.dir", flags.Lookup("cache"))
	viper.BindPFlag("backup.deleteafterupload", flags.Lookup("deleteafterupload"))

	viper.Set("log.name", "pgcloudbackup.log")

	flags.Parse(args)

	return nil
}

func GetHost() string {
	return viper.GetString("database.host")
}

func GetPort() int {
	return viper.GetInt("database.port")
}

func GetUser() string {
	return viper.GetString("database.user")
}

func GetPassword() string {
	return viper.GetString("database.password")
}

func GetDatabaseName() string {
	return viper.GetString("database.name")
}

func GetLogName() string {
	return viper.GetString("log.name")
}

func GetLogFilePath() string {
	return filepath.Join(GetLogPath(), GetLogName())
}

func GetLogPath() string {
	return viper.GetString("log.dir")
}

func GetS3Region() string {
	return viper.GetString("s3.region")
}

func GetS3Bucket() string {
	return viper.GetString("s3.bucket")
}

func GetBackupFilePrefix() string {
	return viper.GetString("backup.fileprefix")
}

func GetBackupCacheDir() string {
	return viper.GetString("backup.dir")
}
func GetDeleteAfterUpload() bool {
	return viper.GetBool("backup.deleteafterupload")
}

func GetGoogleCloudStorageBackupBucket() string {
	return viper.GetString("googlecloud.storage.buckets.backup")
}

func GetGoogleCloudAuthJson() string {
	return viper.GetString("googlecloud.auth.json")
}

func GetLogLevel() log.Level {
	lvl, err := log.ParseLevel(viper.GetString("log.level"))
	if err != nil {
		log.Warnf("Could not get Server Log Level from configuration: %s", err.Error())
		return log.DebugLevel
	}
	return lvl
}
