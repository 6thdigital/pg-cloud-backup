package main

import (
	"fmt"
	"os"
	"os/exec"
	"path"
	"strings"
	"time"

	"bitbucket.org/6thdigital/pg-cloud-backup/config"
	log "github.com/Sirupsen/logrus"
)

const (
	ISO8601Date             = "2006-01-02"
	ISO8601DateTime         = "2006-01-02T15:04:05Z"
	ISO8601DateTimeFileName = "20060102T150405"
)

func createBackup() (*os.File, error) {
	t := time.Now().UTC()
	cmdname := "pg_dumpall"
	filename := fmt.Sprintf("%s-%s.bk", config.GetBackupFilePrefix(), t.Format(ISO8601DateTimeFileName))
	filepath := path.Join(config.GetBackupCacheDir(), filename)
	// port := fmt.Sprintf("%d", config.GetPort())

	//"-h", config.GetHost(), "-p", port,
	args := []string{"-c", "-f", filepath}

	cmd := exec.Command(cmdname, args...)

	log.Infof("%s %s", cmdname, strings.Join(args, " "))

	err := cmd.Run()

	if err != nil {
		log.Errorf("Cannot run pg_dump: %s", err.Error())
		return nil, err
	}

	f, err := os.Open(filepath)

	if err != nil {
		log.Errorf("Cannot open file '%s': %s", filepath, err.Error())
	}
	return f, err
}

func xzCompressFile(file *os.File) (*os.File, error) {
	args := []string{"-z", file.Name()}

	cmd := exec.Command("xz", args...)

	log.Infof("Compressing with 'xz %s'", strings.Join(args, " "))

	err := cmd.Run()

	if err != nil {
		log.Errorf("Cannot run compress with xz: %s", err.Error())
		return nil, err
	}

	filename := fmt.Sprintf("%s.xz", file.Name())

	f, err := os.Open(filename)

	if err != nil {
		log.Errorf("Cannot open file '%s': %s", filename, err.Error())
		return nil, err
	}

	log.Infof("Created compressed file: %s", f.Name())

	return f, nil
}
