package main

import (
	"os"

	"bitbucket.org/6thdigital/pg-cloud-backup/config"
	log "github.com/Sirupsen/logrus"
)

func main() {

	err := config.ParseConfig(os.Args[1:])

	if err != nil {
		log.Fatalln("Could not parse configuration")
		return
	}

	initLogging()

	f, err := createBackup()

	if err != nil {
		log.Fatalf("Could not create pg backup: %s", err.Error())
		return
	}

	err = uploadToGoogleCloudStorage(f.Name(), f)

	if err != nil {
		log.Fatalf("Could not compress pg backup: %s", err.Error())
		return
	}

	if config.GetDeleteAfterUpload() {
		log.Infof("Removing temp file: %s", f.Name())
		err = os.Remove(f.Name())

		if err != nil {
			log.Warnf("Could not remove backup file: %s", err.Error())
			return
		}
	}

	log.Debugln("Completed Backup")
}

func initLogging() {
	// Log as JSON instead of the default ASCII formatter.
	log.SetFormatter(&log.TextFormatter{})

	if config.GetLogPath() == "stdout" {
		log.SetOutput(os.Stdout)
	} else {
		err := os.MkdirAll(config.GetLogPath(), 0755)

		if err != nil {
			log.Errorf("error: ", err)
			log.Fatalf("Could not start logging: " + config.GetLogPath())
		}

		f, err := os.OpenFile(config.GetLogFilePath(), os.O_CREATE|os.O_WRONLY|os.O_APPEND, 0755)

		if err != nil {
			log.Errorf("error: ", err)
			log.Fatalf("Could not create log file at: " + config.GetLogFilePath())
		}

		// Output to file
		log.SetOutput(f)
	}

	// Set Log Level from configuration
	log.SetLevel(config.GetLogLevel())
}
